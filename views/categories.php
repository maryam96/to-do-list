<table class=" infotable" >

<?php foreach ($categories as $category):?>
  <tr>
    <th style="background-color:white; color:Maroon;"> <strong>Category Name</strong></th>
    <th style="background-color:white; color:Maroon;"><strong>Category Description</strong></th>
    <th style="background-color:white; color:Maroon;"><strong>Create list</strong></th>
    <th style="background-color:white; color:Maroon;"><strong>Update category</strong></th>
    <th style="background-color:white; color:Maroon;"><strong>Delete category</strong></th>
  </tr>
  <tr>
    <td>
  <a href="/category.php?categoryId=<?= $category['id'] ?>">
             <?= $category["name"] ?>
           </a>
   </td>
         <td><?= $category["description"] ?></td>
         <td><a href="/createList.php?categoryId=<?= $category['id'] ?>">Add List</a></td>
         <td>
           <a href="/updateCategory.php?categoryId=<?= $category['id'] ?>">Edit</a>
         </td>
         <td>
           <a href="/deleteCategory.php?categoryId=<?= $category['id'] ?>">Delete</a>
         </td>
   </tr>

 <?php endforeach ?>
</table>
