<form action="createCategory.php" method="post">
  <fieldset>
    <div class="form-group">
      <strong><span style="color:Maroon"> Category Name</span></strong> <input class="form-control" name="categoryName" placeholder="Category Name" type="text">
    </div>
    <div class="form-group">
      <strong><span style="color:Maroon"> Category Description</span><input class="form-control" name="categoryDescription" placeholder="Category Description" type="text">
    </div>
    <div>
    <button type="submit" class="btn btn-info">
      Create
    </button>
    </div>
  </fieldset>
</form>
