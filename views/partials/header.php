<!DOCTYPE html>
<html>
    <head>

        <link href="/css/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <?php if (isset($title)): ?>
            <title>To-Do list: <?= $title ?></title>
        <?php else: ?>
            <title>To-do list</title>
        <?php endif ?>



    <body>
        <div class="container">

                <div class="row h-100 justify-content-center">
                    <a href="/"><img alt="To-Do list" src="/img/to-do.jpg"class="img-rounded"  class="img-fluid" /></a>
                    <br>
                </div>

                <?php if (!empty($_SESSION["id"])): ?>
                    <ul >
                        <li><a  href="/"><button class="btn btn-info">Categories</button></a></li>
                        <li><a  href="/createCategory.php"><button class="btn btn-info">Create Category</button></a></li>
                        <li><a  href="/resetpassword.php"><button class="btn btn-info">reset Password</button></a></li>
                        <li><a href="/logout.php"><strong><button class="btn btn-info">Log Out</button></strong></a></li>
                    </ul>
                <?php endif ?>
              </div>



</body>
