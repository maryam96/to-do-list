<br>
<div class="container">
<form align="center"  action="login.php" method="post">
  <fieldset>
    <div class="form-group">
      <input  autocomplete="off"  name="username" placeholder="Username" type="text" >
    </div class="form-group">
    <div class="form-group">
      <input  name="password" placeholder="Password" type="password" >
    </div>
    <div class="checkbox">
    <label><input  type="checkbox"> Remember me</label>
  </div>
    <div class="form-group">
      <button   type="submit"  class="btn btn-info">
        <span aria-hidden="true" class="glyphicon glyphicon-log-in"></span>
        login
      </button>
    </div>
  </fieldset>
</form>
<div  align="center">
    or <a href="register.php">register</a> for an account
</div>
</div>
</div>
