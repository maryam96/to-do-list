<table style="border-style:solid; border-width:5px; border-color:Maroon;" class="infotable ">
  <tr>
    <th style="background-color:white; color:Maroon;"> Category Name</th>
    <th  style="background-color:white; color:Maroon;" >Category Description</th>
  </tr>
  <tr>
    <td><?= $category['name'] ?></td>
    <td><?= $category['description'] ?></td>
  </tr>
</table>

<br>
<div align="center">
  <h2 style="color:Maroon"><strong>Category lists</strong></h2>
</div>
<table style="border-style:solid; border-width:5px; border-color:Maroon;" class="infotable">

  <tr>
      <th style="background-color:white; color:Maroon;">List Name</th>
      <th style="background-color:white; color:Maroon;">List Description</th>
      <th style="background-color:white; color:Maroon;">Create new task</th>
      <th style="background-color:white; color:Maroon;">Edit list</th>

  </tr>
  <?php foreach ($list as $lists): ?>
  <tr>
    <td><a href="/to-do.php?listId=<?= $lists['id'] ?>">
      <?= $lists["name"] ?>
    </a>
    <td> <?= $lists["description"] ?> </td>
    <td><a  href="/createTo-Do.php?listId=<?= $lists['id'] ?>">Add to-do </a></td>
    <td><a  href="/updateList.php?listId=<?= $lists['id'] ?>">Edit </a></td>
  </tr>
<?php endforeach ?>
</table>
