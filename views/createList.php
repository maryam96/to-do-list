<form action="createList.php" method="post">
  <fieldset>
    <div class="form-group">
      <strong><span style="color:blue">list Name</span></strong> <input class="form-control" name="listName" placeholder="Category Name" type="text">
    </div>
    <div class="form-group">
      <strong><span style="color:blue">list Description</span><input class="form-control" name="listDescription" placeholder="Category Description" type="text">
    </div>
    <input type="hidden" name="categoryId" value="<?= $categoryId ?>">
    <div>
    <button type="submit" class="btn btn-info">
      Create
    </button>
    </div>
  </fieldset>
</form>
