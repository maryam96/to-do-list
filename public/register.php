<?php
//configuration
require("../includes/config.php");
//if user reached page via get method
if ($_SERVER["REQUEST_METHOD"]=="GET")
{
  render("register.php",["title"=>"Register"]);
}
//if user reache page via post method
elseif ($_SERVER["REQUEST_METHOD"]=="POST") {
   if(empty($_POST["username"]))
   {
     apologize("You must enter a username.");
   }
   elseif (empty($_POST["password"])) {
     apologize("You must enter a password.");
   }
   elseif (empty($_POST["confirmation"])) {
     apologize("You must confirm your password.");
   }
   elseif ($_POST['password']!= $_POST['confirmation']) {
     apologize("Your password and password confirmation do not match.");
   }
   else {
   $username=$_POST["username"];
   $hash=password_hash($_POST["password"], PASSWORD_DEFAULT);
   $register=query("INSERT IGNORE INTO user (username,hash) VALUES('$username','$hash')");
   $row=query("SELECT id FROM user ORDER BY id DESC LIMIT 1;");
   $id = $row[0]['id'];
   $_SESSION["id"] = $id;
   redirect("/");
}}
 ?>
