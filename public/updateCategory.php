<?php
    // configuration
    require("../includes/config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    { $categoryId=$_GET['categoryId'];
      $category=query("SELECT * FROM category WHERE id=$categoryId");
        render("updateCategory.php", ["title" => "update category", "category"=>$category,"categoryId"=>$categoryId]);
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["categoryname"]))
        {
            apologize("You must enter a name.");
        }
        else if (empty($_POST["categorydescription"]))
        {
            apologize("You must enter a description.");
        }
        else
        {
           $name=$_POST["categoryname"];
           $description=$_POST["categorydescription"];
           $updatedcategory= query("UPDATE category SET name = '$name', description= '$description' WHERE id=$categoryId");
        }
        if($updatedcategory)
            {
                redirect("/");
            }
            else
            {
                apologize("Sorry, you can not update category.");
            }
        }


?>
