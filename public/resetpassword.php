<?php
//configuration
require("../includes/config.php");
//if user reached page via get method
if ($_SERVER["REQUEST_METHOD"]=="GET")
{ $userid=$_SESSION["id"];
  render("resetpassword.php",["title"=>"resetpassword"]);
}
//if user reache page via post method
else if ($_SERVER["REQUEST_METHOD"]=="POST")
{
  if (empty($_POST["currpass"]))
        {
            apologize("You must provide your current password.");
        }
      else  if (empty($_POST["Newpass"]))
              {
                  apologize("You must provide your new  password.");
              }
      else  if (empty($_POST["confirm"]))
                    {
                        apologize("You must confirm your  new password.");
                    }
        else {

          $user= query("SELECT * FROM user WHERE id = '$userid' ");
          if (count($user) == 1)
          {

              $row = $user[0];


              if (password_verify($_POST["currpass"], $row["hash"]))
              {
                if ($_POST['Newpass']= $_POST['confirm']) {
                  $hash=password_hash($_POST["Newpass"], PASSWORD_DEFAULT);
                  $_SESSION["id"] = $row["id"];
                  $updatepassword=query("UPDATE user SET hash=$hash");
                    redirect("/");

                }
              else
                {
                  apologize("Your password and password confirmation do not match.");
                }

              }
              else {
                apologize("Invalid password.");
              }
          }
          apologize("sorry,there is something wrong");

      }
    }
?>
