<?php
require("../includes/config.php");
$categoryId=$_GET['categoryId'];
$userid=$_SESSION["id"];
if (empty($categoryId))
{
     apologize("Please choose the category");
}
else
{
  $category=query("SELECT * FROM category WHERE id=$categoryId");
  $category=$category[0];
  $list=query("SELECT * FROM list WHERE category_id=$categoryId");
    render("category.php", ["title" => "Category " . $category['name'], "category" => $category,"list"=>$list]);
}

 ?>
