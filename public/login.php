<?php
//configuration
require("../includes/config.php");
//if user reached page via get method
if ($_SERVER["REQUEST_METHOD"]=="GET")
{
  render("login.php",["title"=>"Login"]);
}
//if user reache page via post method
else if ($_SERVER["REQUEST_METHOD"]=="POST")
{
  if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must provide your password.");
        }
        else {
          $username = $_POST['username'];

          $user= query("SELECT * FROM user WHERE username = '$username' ");


          if (count($user) == 1)
          {

              $row = $user[0];


              if (password_verify($_POST["password"], $row["hash"]))
              {

                  $_SESSION["id"] = $row["id"];

                  redirect("/");
              }
          }


          apologize("Invalid username and/or password.");
        }
}

?>
