<?php
//configuration
require("../includes/config.php");
//if user reached page via get method
if ($_SERVER["REQUEST_METHOD"]=="GET")
{
  render("createCategory.php",["title"=>"Create Category"]);
}
//if user reache page via post method
elseif ($_SERVER["REQUEST_METHOD"]=="POST")
{ if(empty($_POST["categoryName"]))
  {
    apologize("you must enter a category name.");
  }
  else if(empty($_POST["categoryDescription"]))
  {
    apologize("you must enter a category description. ");
  }
  else{
  $categoryName=$_POST["categoryName"];
  $categoryDescription=$_POST["categoryDescription"];
  $category=query("INSERT INTO category (name, description) VALUES ('$categoryName','$categoryDescription')");
  }

if ($category)
   {
     redirect("/");
   }
  else
   {
      apologize("sorry,can not create category.");
   }
}

 ?>
