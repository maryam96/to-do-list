<?php

    // configuration
    require("../includes/config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    { $listId=$_GET['listId'];
      $list=query("SELECT * FROM list WHERE id=$listId");
        render("updateList.php", ["title" => "update List", "list"=>$list,"listId"=>$listId]);
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["listname"]))
        {
            apologize("You must enter a name.");
        }
        else if (empty($_POST["listdescription"]))
        {
            apologize("You must enter a description.");
        }
        else
        {
           $name=$_POST["listname"];
           $description=$_POST["listdescription"];
           $updatedlist= query("UPDATE list SET name = '$name', description= '$description' WHERE id=$listId");
        }
        if($updatedlist)
            {
                redirect("/");
            }
            else
            {
                apologize("Sorry, you can not update list.");
            }
        }


?>
